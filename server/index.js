const http = require('http');
const { PORT = 8080 } = process.env;

const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
app.use(express.static("public"));

app.get('/', (req, res) => {
  res.sendFile('index.html');
})

app.get('/cars', (req, res) => {
  res.sendFile("sewa.html", {root: "./public"});
})

app.listen(PORT, function() {
  console.log('Server berjalan di http://127.0.0.1:' + PORT + '/');
})


