class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <img class="car-img" src="${this.image}" alt="${this.manufacture}" width="64px">
      <p>${this.manufacture} ${this.model}/${this.type}</p>
      <p><b>Rp ${this.rentPerDay}/hari</b></p>
      <p>${this.description}</p>    
      
      <div class="row">
        <img src="assets/icon/fi_users.png" alt="" class="car-icon col-2">
        <p class="col-10">${this.capacity} orang</p>
      </div>
      <div class="row">
        <img src="assets/icon/fi_settings.png" alt="" class="car-icon col-2">
        <p class="col-10">${this.transmission}</p>
      </div>
      <div class="row">
        <img src="assets/icon/fi_calendar.svg" alt="" class="car-icon col-2">
        <p class="col-10">Tahun ${this.year}</p>
      </div>
      
      <button class="btn btn-bg">Pilih Mobil</button>
    `;
  }
}